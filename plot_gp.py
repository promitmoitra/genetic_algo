from matplotlib import pyplot as plt
import networkx as nx
import numpy as np
import pickle
from deap import base,creator,gp,tools
import gp_bessel as gpb
import sys

cxprb=0.9
mutprb=0.1

with open("./data/sniff_test_hof_{0}-{1}.pkl".format(cxprb,mutprb),"rb") as datafile:
    hof_data = pickle.load(datafile)

hof = hof_data["halloffame"]

##print("Fitness:",hof[int(sys.argv[1])].fitness.values)
##
##nodes,edges,labels = gp.graph(hof[int(sys.argv[1])])
##
##numlabels={}
##for key,value in labels.items():
##    numlabels[key]=key
##
##for key,value in labels.items():
##    if isinstance(value,float):
##        labels[key]=np.around(value,3)

##import pygraphviz as pgv
##g = pgv.AGraph()
##g.add_nodes_from(nodes)
##g.add_edges_from(edges)
##g.layout(prog="dot")
##
##for i in nodes:
##    n = g.get_node(i)
##    n.attr["label"] = labels[i]
##
##g.draw("tree.pdf")

def subexpr(ind,node):
    sli=ind.searchSubtree(node)
    subind=ind[sli]
    subind=creator.Individual(subind)
    subfunc=gp.compile(subind,gpb.pset)
    return subind,subfunc

def draw_tree(ind,num=False):
    nodes,edges,labels = gp.graph(ind)
    
    numlabels={}
    for key,value in labels.items():
        numlabels[key]=key
    
    for key,value in labels.items():
        if isinstance(value,float):
            labels[key]=np.around(value,3)

    fig=plt.figure()
    g = nx.Graph()
    g.add_nodes_from(nodes)
    g.add_edges_from(edges)
    pos = nx.nx_agraph.graphviz_layout(g, prog="dot")
    
    nx.draw_networkx_nodes(g,pos,node_color='w',node_size=1000,edgecolors='k')
    nx.draw_networkx_edges(g,pos)
    if num==True:
        nx.draw_networkx_labels(g,pos,numlabels,font_size=17)
    else:    
        nx.draw_networkx_labels(g,pos,labels,font_size=17)
    return

def plotf(ind):
    f = plt.figure()
    ##sol='add(mul(y0(x), mul(x, x)), mul(y1(x), x))'
    ##sol='sub(mul(y0,-1),div(y1,x))'
    ##func = gp.compile(expr=gpb.creator.Individual.from_string(sol,gpb.pset),pset=gpb.pset)
    
    func = gp.compile(expr=str(ind),pset=gpb.pset)
    x = gpb.xarr#np.arange(0,100,0.01)
    fx = func(x)#[func(i) for i in x]
    plt.plot(x,gpb.fy2(x),'-',label="original -y''")
    plt.plot(x,fx,'.',label="evolved")
    plt.legend()
    return

##plt.show()
