import operator
import math
import random

import numpy as np
import mpmath as mp
import scoop
from scoop import futures

from deap import algorithms
from deap import base
from deap import creator
from deap import tools
from deap import gp

##def pdiv(l,r):
##    res=[]
##    for i in range(len(r)):
##        if r[i]==0:
##            res.append(np.inf)
##        else:
##            res.append(l[i]/r[i])
##    return np.array(res)

j0 = np.vectorize(mp.besselj,otypes=[float])

##def y0(x):
##    return j0(0,x,derivative=0)
##
##def y1(x):
##    return j0(0,x,derivative=1)
##
def fy2(x):
    return j0(0,x,derivative=2)	

xarr = np.arange(0.0001,100,0.01)
y0 = j0(0,xarr,derivative=0)
y1 = j0(0,xarr,derivative=1)
y2 = j0(0,xarr,derivative=2)

pset = gp.PrimitiveSet("MAIN",1)
pset.addPrimitive(operator.add,2)
pset.addPrimitive(operator.sub,2)
pset.addPrimitive(operator.mul,2)
pset.addPrimitive(np.divide,2,name="div")
##pset.addPrimitive(pdiv,2)
##pset.addPrimitive(y0,1)
##pset.addPrimitive(y1,1)
pset.addTerminal(y0,"y0")
pset.addTerminal(y1,"y1")
pset.renameArguments(ARG0='x')
##pset.addPrimitive(operator.neg,1)
pset.addEphemeralConstant("rand101", lambda: random.uniform(-1,1))
##if not scoop.IS_ORIGIN:
##    pset.addEphemeralConstant("rand", lambda: random.uniform(-1,1))

##creator.create("FitnessMin",base.Fitness,weights=(-1.0,))
##creator.create("Individual",gp.PrimitiveTree,fitness=creator.FitnessMin)
creator.create("FitnessMax",base.Fitness,weights=(1.0,))
creator.create("Individual",gp.PrimitiveTree,fitness=creator.FitnessMax)

toolbox = base.Toolbox()
toolbox.register("map",futures.map)
toolbox.register("expr",gp.genHalfAndHalf,pset=pset,min_=1,max_=2)
##toolbox.register("expr",gp.genFull,pset=pset,min_=1,max_=3)
toolbox.register("individual",tools.initIterate,creator.Individual,toolbox.expr)
toolbox.register("population",tools.initRepeat,list,toolbox.individual)
toolbox.register("compile",gp.compile,pset=pset)

def evaluate(individual,x):
    func = toolbox.compile(expr=individual)
    sqerrors = ((func(x)-y2)**2)
    sstot = ((np.mean(y2)-y2)**2)
##    res = math.fsum(sqerrors)/len(x)
    res = 1-(math.fsum(sqerrors)/math.fsum(sstot))
    if np.isnan(res):
        res = -np.inf
    return res,

toolbox.register("evaluate",evaluate,x=xarr)
##toolbox.register("select",tools.selTournament,tournsize=3)
toolbox.register("select",tools.selDoubleTournament,fitness_size=3,parsimony_size=1.2,fitness_first=True)
toolbox.register("mate",gp.cxOnePoint)
toolbox.register("expr_mut",gp.genFull,min_=1,max_=2)
toolbox.register("mutate",gp.mutUniform,expr=toolbox.expr_mut,pset=pset)

toolbox.decorate("mate",gp.staticLimit(key=operator.attrgetter("height"),max_value=6))
toolbox.decorate("mutate",gp.staticLimit(key=operator.attrgetter("height"),max_value=6))

def sniff1(ind):
    fstr=str(ind)
    fset=['x','y0','y1']
    rnd=np.random.uniform(0.9,1.1)
    fsniff=[]
    for func in fset:
        if fstr.find(func)!=-1:
            fsniff.append(fstr.replace(func,'mul({0},{1})'.format(func,rnd)))
            fsniff.append(fstr.replace(func,'div({0},{1})'.format(func,rnd)))
            fsniff.append(fstr.replace(func,'add({0},sub({1},1))'.format(func,rnd)))
            fsniff.append(fstr.replace(func,'sub({0},sub({1},1))'.format(func,rnd)))
        else:
            continue
    return fsniff

def sniff2(ind):
    fset=['x','y0','y1']
    fstr=str(ind)
    fsniff=[]
    for func in fset:
        if fstr.find(func)!=-1:
            frep=[];frep[:]=fset
            frep.remove(func)
            for i in frep:
                fsniff.append(fstr.replace(func,i))
        else:
            continue
    return fsniff

def sniff3(ind):
    fsniff=[]
    newind=gp.mutEphemeral(ind,'all')[0]
    fsniff.append(str(newind))
    return fsniff

def sniff4(ind):
    fsniff=[]
    iprims=[]
    if len(ind) < 3 or ind.height <= 1:
        fsniff.append(str(ind))
        return fsniff
    
    for i,node in enumerate(ind[1:],1):
        if isinstance(node,gp.Primitive):
            iprims.append((i,node))
    if len(iprims)!=0:
        index,prim = random.choice(iprims)
        sli=ind.searchSubtree(index)
        subind = creator.Individual(ind[sli])
        nodes,edges,labels = gp.graph(ind)
        for i in edges:
            if i[1]==index:
                parent=i[0]
        pname = ind[parent].name
        if pname in ['add','sub']:
            newind = str(ind).replace(str(subind),'0')
            fsniff.append(newind)
        elif pname=='mul':
            newind = str(ind).replace(str(subind),'1')
            fsniff.append(newind)
        elif pname=='div':
            children=[]
            for i in edges:
                if i[0]==parent:
                    children.append(i[1])
            if index==min(children):
                newind = str(ind).replace(str(subind),'0')
                fsniff.append(newind)
            else:
                newind = str(ind).replace(str(subind),'1')
                fsniff.append(newind)
        rnd = np.random.uniform(0.9,1.1)
        fsniff.append(str(ind).replace(str(subind),'mul('+str(subind)+', {0})'.format(rnd)))
        fsniff.append(str(ind).replace(str(subind),'div('+str(subind)+', {0})'.format(rnd)))
        fsniff.append(str(ind).replace(str(subind),'add('+str(subind)+', {0})'.format(rnd-1)))
        fsniff.append(str(ind).replace(str(subind),'sub('+str(subind)+', {0})'.format(rnd-1)))
    return fsniff                               

def sniff5(ind):
    fsniff = []
    fstr = str(ind)
    rnd = np.random.uniform(0.9,1.1)
    fsniff.append('add('+fstr+', {0})'.format(rnd-1))
    fsniff.append('sub('+fstr+', {0})'.format(rnd-1))
    fsniff.append('mul('+fstr+', {0})'.format(rnd))
    fsniff.append('div('+fstr+', {0})'.format(rnd))
    return fsniff

def fitcheck(i):
    return i.fitness.values

def main():
##    random.seed(318)

    population = toolbox.population(n=1000)
    hof = tools.HallOfFame(100)
    cxprb = 0.9
    mutprb = 0.2
    ngen = 300

    for gen in range(ngen):
        avgfit=0;maxfit=0
        for i in population:
            if i.fitness.valid:
                avgfit+=i.fitness.values[0]
                if i.fitness.values[0]>maxfit:
                    maxfit=i.fitness.values[0]
        avgfit=avgfit/len(population)
        print(gen,np.around(maxfit,5),np.around(avgfit,5),len(population))
        population = algorithms.varAnd(population, toolbox, cxpb=cxprb, mutpb=mutprb)

        # Evaluate the individuals with an invalid fitness
        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        hof.update(population)
##        record = stats.compile(population)
##        logbook.record(gen=gen, evals=len(invalid_ind), **record)
        population = toolbox.select(population, k=300)#len(population))

        sniffind=[]
        if gen % 5 == 0:
            print("sniffing...")
            popcopy = population.copy()
            popcopy.sort(key=fitcheck,reverse=True)
            bestind = random.choice(popcopy[0:10])
            sniffind = sniff1(bestind)
            sniffind += sniff2(bestind)
            sniffind += sniff3(bestind)
            sniffind += sniff4(bestind)
            sniffind += sniff5(bestind)
            newinds = []
            for i in sniffind:
##                print(type(i))
                newind = creator.Individual.from_string(i,pset)
                newinds.append(newind)
            newfits = toolbox.map(toolbox.evaluate,newinds)
            for ind,fit in zip(newinds,newfits):
                ind.fitness.values=fit
            for i in newinds:
                if i.fitness.values>bestind.fitness.values:
                    population.append(i)

##    pop,log = algorithms.eaSimple(pop, toolbox, cxprb, mutprb, gen,
##                                   halloffame=hof, verbose=True)
##    pop,log = algorithms.eaMuPlusLambda(pop,toolbox,300,150,cxprb,mutprb,gen,
##                                        halloffame=hof,verbose=True)
    return cxprb,mutprb,population,hof

if __name__ == "__main__":
    cxprb,mutprb,pop,hof=main()
    
    import pickle
    hof_data = dict(halloffame=hof)
    with open("sniff_test_hof_{0}-{1}.pkl".format(cxprb,mutprb),"wb") as datafile:
        pickle.dump(hof_data,datafile)

