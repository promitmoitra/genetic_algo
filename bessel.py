import mpmath as mp
#j0 = lambda x: besselj(0,x)
#j01 = lambda x: besselj(0,x,derivative=1)
#j02 = lambda x: besselj(0,x,derivative=2)
#j1 = lambda x: besselj(1,x)
#j2 = lambda x: besselj(2,x)
#j3 = lambda x: besselj(3,x)
#plot([j0,j1,j2,j3],[0,20])
#plot([j0],[0,200])

from matplotlib import pyplot as plt
import numpy as np

j0 = np.vectorize(mp.besselj,otypes=[float])
x = np.arange(0,20,0.1)
y0 = j0(1,x,derivative=0)
y1 = j0(1,x,derivative=1)
y2 = j0(1,x,derivative=2)
fig,ax = plt.subplots()
ax.plot(x,y0,label='b0')
ax.plot(x,y1,label='b1')
ax.plot(x,y2,label='b2')
ax.legend()
plt.show()
