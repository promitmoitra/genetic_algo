import numpy as np
import networkx as nx
from deap import base,creator

##creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

def lmap(r,x):
    return r*x*(1-x)

def gen_net(n,k,p):
##    adj = np.zeros((n,n))
##    G = nx.gnp_random_graph(n,p,seed=183)
    G = nx.connected_watts_strogatz_graph(n,k,p,seed=183)
    adj = nx.to_numpy_matrix(G)
    adj = np.array(adj)
    return G,adj

def nbhood(adj,r,x,i):
    nb = []
    r_nb = []
    nn = np.nonzero(adj[i])[0]
    for idx in nn:
        nb.append(np.take(x,idx))
        r_nb.append(np.take(r,idx))
    return np.array(r_nb),np.array(nb)

def check_sync(x):
##    return np.sqrt(np.mean([(np.mean(x)-i)**2 for i in x]))
    theta = 2*np.pi*x
    phi = np.mean(np.cos(theta)+1j*np.sin(theta))
    return np.absolute(phi),np.angle(phi)    

def check_local_sync(nbrs,xi):
    x = np.array(list(nbrs)+[xi])
##    return np.sqrt(np.mean([(np.mean(x)-i)**2 for i in x]))
    theta = 2*np.pi*x
    phi = np.mean(np.cos(theta)+1j*np.sin(theta))
    return np.absolute(phi)    

def evaluate(r,N,k,eps):
##    N = 10
##    eps = 0.2
##    k = 10
    p = 0.0
    trans=200;obs=100;T = trans+obs
    icnum = 5
    sync_mean = []
    G,adj = gen_net(N,k,p)
    for ic in range(icnum):
        x = np.random.random(N)
        fx = np.zeros(N)
        sync = []##
        for t in range(T):
            ##sync=[]
            for i in range(N):
                r_nbhs,nbhs = nbhood(adj,r,x,i)
                sync.append(check_local_sync(nbhs,x[i]))
                fx[i] = (1-eps)*lmap(r[i],x[i]) + eps*np.mean(lmap(r_nbhs,nbhs))
            x = np.copy(fx)
##            if t>=trans:             
##                sync_mean.append(np.mean(sync))
        sync_mean.append(np.mean(sync[N*trans:]))##
    return np.mean(sync_mean),

def evalplot(r):
    rec = np.zeros((T,N))
    x = np.random.random(N)
    fx = np.zeros(N)
    for t in range(T):
        for i in range(N):
            r_nbhs,nbhs = nbhood(adj,r,x,i)
            fx[i] = (1-eps)*lmap(r[i],x[i]) + eps*np.mean(lmap(r_nbhs,nbhs))
        rec[t,:]=np.copy(x)
        x = np.copy(fx)
    return rec,adj

def tprofile(rec,t):
    fig,ax = plt.subplots()
    blues=plt.get_cmap('Blues')
    ax.set_xlim(0,100)
    ax.set_ylim(0,1)
    ax.set_xlabel("$Site\ (i)$",fontsize=20)
    ax.set_ylabel("$Values\ (x^{{i}}_{{t}})$",fontsize=20)
    ax.set_title("$\\varepsilon={0},\ k={1}$".format(eps,k),fontsize=26)
    n = len(rec[0])
    for i in rec[-t:]:
        ax.plot(range(n),i,'k.')#,c=blues(i/50))
    return

def stplot(rec):
    fig,ax=plt.subplots()
    ax.set_xlabel("$Site\ (i)$",fontsize=20)
    ax.set_ylabel("$Time$",fontsize=20)
    norm = mpl.colors.Normalize(vmin=0.0,vmax=1.0)    
    im=ax.imshow(rec[-100:],aspect='equal',interpolation='none',origin='lower',norm=norm)
    cbar=fig.colorbar(im)
    ax.set_title("$\\varepsilon={0},\ k={1}$".format(eps,k),fontsize=26)
    return

def distrib(r):
    fig,ax=plt.subplots()
    ax.set_xlabel("$Values\ r_{{i}}$",fontsize=20)
    ax.set_ylabel("$Frequency$",fontsize=20)
    y,x = np.histogram(r,bins=np.arange(3.5,4,0.01))
    centers=0.5*(x[1:]+x[:-1])
    ax.plot(centers,y,'.-')
##    ax.semilogy(centers,y,'.-')
##    ax.loglog(centers,y,'.-')    
    return

def sync_plot_global(rec,t):
    fig,ax=plt.subplots(1,2)
    ax[0].set_ylim(0,1)
    ax[1].set_ylim(-np.pi,np.pi)
    ampop=[]
    argop=[]
    for i in rec[-t:]:
        R,Th = check_sync(i)
##        R = check_sync(i)
        ampop.append(R)
        argop.append(Th)
    ax[0].plot(range(t),ampop,'.-')
    ax[1].plot(range(t),argop,'.-')
    return

def sync_plot_local(rec,t):
    sync_mean=[]
    for state in rec[-t:]:
        sync=[]
        for i in range(N):
            r_nbhs,nbhs=nbhood(adj,r,state,i)
            sync.append(check_local_sync(nbhs,state[i]))
        sync_mean.append(np.mean(sync))
    plt.ylim(0,1);plt.plot(range(t),sync_mean)
    return

if __name__=='__main__':
##    pass
    
    N=100;k=10;p=0.0
    trans=1000;obs=100;T=trans+obs
    eps=0.3
    
    G,adj = gen_net(N,k,p)
    
    import pickle
    with open("net_hof.pkl","rb") as datafile:
        hof_data = pickle.load(datafile)
    hof = hof_data["halloffame"]
    print("Fitness:",hof[0].fitness.values)

    r = np.array(hof[0])
##    np.random.shuffle(r)
##    r = np.ones(N)*4.0
##    r = []
##    for i in range(N):
##        r.append(np.random.uniform(3.5,4))

    rlist=[]
    for i in hof:
        rlist+=list(i)

    rec = evalplot(r)
    
    from matplotlib import pyplot as plt
    import matplotlib as mpl

