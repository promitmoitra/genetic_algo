from deap import base,creator,tools,algorithms
import random
##import numpy as np
import logi_net as lnet
from scoop import futures

##creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()
toolbox.register("map",futures.map)

N=500
k=10
eps=0.3
##step=1e-5
##rmin=3.0
##rmax=4.0+step
####params = np.around(np.arange(rmin,rmax,step),decimals=5)
##params = np.arange(rmin,rmax,step)
##toolbox.register("r",np.random.choice,params)
toolbox.register("r",random.uniform,3.5,4.0)
toolbox.register("individual",tools.initRepeat,creator.Individual,
                 toolbox.r,n=N)
toolbox.register("population",tools.initRepeat,list,toolbox.individual)

def mutUniform(individual,low,up,indpb):
    size = len(individual)
    for i in range(size):
        if random.random() < indpb:
            individual[i] = random.uniform(low,up)
    return individual,

toolbox.register("evaluate",lnet.evaluate,N=N,k=k,eps=eps)
toolbox.register("select",tools.selTournament,tournsize=3)
toolbox.register("mutate",mutUniform,low=3.5,up=4.0,indpb=0.1)
toolbox.register("mate",tools.cxTwoPoint)

def main():
    pop = toolbox.population(n=300)
    hof = tools.HallOfFame(100)
    cx = 0.9
    mut = 0.2
    gen = 100
    
    pop,log = algorithms.eaSimple(pop,toolbox,cx,mut,gen,
                                    halloffame=hof,verbose=True)
    return pop,hof

if __name__ == '__main__':
    pop,hof=main()
    import pickle
    hof_data = dict(halloffame=hof)
    with open("net_hof_N-500_eps-3.pkl","wb") as datafile:
        pickle.dump(hof_data,datafile)

